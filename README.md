# Project Models

This provides a module that generates data models for a project checks using 
the [@quenk/dagen][1] tool.

## Introduction

Data Models are usued for manipulating data stored in some datastore.
In this case, MongoDB. The classes generated here are extensions of the
`@quenk/dback-model-mongodb` BaseModel class.

This module generates a module for each of the data models defined in your
projects's $PROJECT_SCHEMA_DIR/models folder using the "checks" property.

## Installation
This module should be included in your project directly and it's `build.mk` file
added to your Makefile.

## Usage
This module will generate a class for every schema found in your
$PROJECT_SCHEMA_MODELS_DIR folder (should be set in the Makefile or elsewhere).

An additional index file will be generated with a `getInstance()` function that
allows instances of your models to be dynamically retrieved.

Example:

```
import {getInstanceOf} from '@myproject/models';

let model = getInstanceOf(mongo, 'user');

// Do things with model

```

[1]: https://github.com/quenktechnologies/dagen
